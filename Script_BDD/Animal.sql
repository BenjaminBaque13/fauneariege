-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 17 Mai 2019 à 15:05
-- Version du serveur :  5.7.26-0ubuntu0.18.04.1
-- Version de PHP :  7.2.17-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `exam_web_dyn`
--

-- --------------------------------------------------------

--
-- Structure de la table `Animal`
--

CREATE TABLE `Animal` (
  `id` int(11) NOT NULL,
  `famille` varchar(50) NOT NULL,
  `espece` varchar(50) NOT NULL,
  `nom_usuel` varchar(50) NOT NULL,
  `statut` varchar(50) NOT NULL,
  `nombre` int(11) NOT NULL,
  `texte` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `Animal`
--

INSERT INTO `Animal` (`id`, `famille`, `espece`, `nom_usuel`, `statut`, `nombre`, `texte`) VALUES
(15, 'CervidÃ©', 'Cerf Ã©laphe', 'Cerf', 'Sauvage', 1000, 'Le cerf Ã©laphe (Cervus elaphus) est un grand cervidÃ© des forÃªts tempÃ©rÃ©es d\'Europe, d\'Afrique du Nord, d\'AmÃ©rique du Nord et d\'Asie. Son nom est un plÃ©onasme car Â« Ã©laphe Â» signifie dÃ©jÃ  Â« cerf Â» en grec.\r\n\r\nCe cerf est l\'un des reprÃ©sentants les plus connus de cette famille de mammifÃ¨res. L\'espÃ¨ce comprend plusieurs sous-espÃ¨ces, dont le cerf de Bactriane, le cerf du Turkestan, le cerf rouge du Turkestan ou encore le cerf Ã©laphe du Turkestan.\r\n\r\nLa femelle est la biche et le petit, jusqu\'Ã  l\'Ã¢ge de 6 mois, le faon. Ensuite, de 6 mois Ã  1 an, on l\'appelle Â« bichette Â» si c\'est une femelle et Â« hÃ¨re Â» s\'il s\'agit d\'un mÃ¢le. De 1 an Ã  2 ans, le jeune mÃ¢le est appelÃ© Â« daguet Â» avec deux grands bois secs. '),
(25, 'CanidÃ©', 'Chien', 'Patou', 'Domestique', 43, 'Le chien de montagne des PyrÃ©nÃ©es ou montagne des PyrÃ©nÃ©es (dit en langage courant pastou ou patou) est une race ancienne de chien de berger, utilisÃ© dans le Sud-ouest de la France et le Nord-est de l\'Espagne, en particulier les PyrÃ©nÃ©es pour la protection des troupeaux contre les prÃ©dateurs, notamment les ours qui vivent ici. Chien de grande taille, fortement charpentÃ©, Ã  poils longs et robe blanche, il est mentionnÃ© dans des Ã©crits dÃ¨s le XIVe siÃ¨cle et le standard de race est fixÃ© au dÃ©but du XXe siÃ¨cle.\r\n\r\nÃ‰levÃ© et socialisÃ© en famille, c\'est aussi un chien de garde ou un chien de compagnie. '),
(26, 'FÃ©lidÃ©', 'Chat', 'Siamois', 'Domestique', 20, 'Le Siamois est une race de chat originaire de ThaÃ¯lande.\r\n\r\nLes premiers Siamois sont arrivÃ©s en Grande-Bretagne au milieu du XIXe siÃ¨cle. Leur succÃ¨s est tel qu\'ils arrivent vite aux Ã‰tats-Unis. Ã€ cette Ã©poque, seuls les seal point Ã©taient reconnus mais petit Ã  petit sâ€™est ajoutÃ©e une trÃ¨s large palette de couleurs. La race a connu une grande Ã©volution dans les annÃ©es 1970 quand les Ã©leveurs ont accentuÃ© ses caractÃ©ristiques physiques. Les AmÃ©ricains ont recherchÃ© un corps plus Ã©lÃ©gant et une tÃªte plus longue. Les Anglais, eux, prÃ©fÃ©rÃ¨rent travailler le positionnement des oreilles, plus basses et plus grandes, et la forme orientale des yeux. Aujourdâ€™hui, les meilleurs Siamois combinent les qualitÃ©s des chats amÃ©ricains et des chats anglais.\r\n\r\nL\'origine se trouve dans le Sud-Est de l\'Asie : la race pourrait descendre des chats sacrÃ©s des temples de Siam (d\'oÃ¹ leur nom). Ces chats Ã©taient dÃ©jÃ  reprÃ©sentÃ©s dans certains manuscrits retrouvÃ©s Ã  Ayutthaya (ancienne capitale du Siam) et datant de 1350. Un naturaliste allemand a Ã©galement dÃ©crit des chats aux extrÃ©mitÃ©s foncÃ©es dans le courant du XIXe siÃ¨cle. Ils Ã©taient Ã©galement dÃ©gustÃ©s pour la qualitÃ© de leur viande.\r\n\r\nOn dit que ce chat Ã©tait sacrÃ© pour les rois du Siam et qu\'ils Ã©taient prÃ©cieusement gardÃ©s au palais et rÃ©servÃ©s aux membres de la famille royale. D\'autres lÃ©gendes racontent au contraire qu\'Ã  l\'arrivÃ©e des Anglais et des FranÃ§ais, le roi avait cachÃ© ses chats prÃ©fÃ©rÃ©s, des Khao Manee, et donna des Siamois en cadeaux aux Ã©trangers en les faisant passer pour sacrÃ©s et Ã©viter ainsi d\'Ãªtre volÃ© de ses vÃ©ritables chats porte-bonheur. '),
(30, 'Suidae', 'Sanglier d\'europe', 'Sanglier', 'Sauvage', 2000, 'Le sanglier d\'Europe, sanglier d\'Eurasie ou plus simplement sanglier (Sus scrofa), est une espÃ¨ce de mammifÃ¨res omnivores, forestiers de la famille des SuidÃ©s. Cette espÃ¨ce abondamment chassÃ©e est aussi considÃ©rÃ©e comme une espÃ¨ce-ingÃ©nieur, capable de dÃ©velopper des stratÃ©gies d\'adaptation Ã  la pression de chasse, ce qui lui confÃ¨re parfois un caractÃ¨re envahissant.\r\n\r\nLe porc (ou cochon) est une sous-espÃ¨ce domestique du sanglier. '),
(31, 'CamÃ©lidÃ©', 'Lama', 'Lama', 'Semi-domestique', 67, 'Le lama blanc ou lama (Lama glama) est un camÃ©lidÃ© domestique d\'AmÃ©rique du Sud. Sa longÃ©vitÃ© est comprise entre 10 et 20 ans.\r\n\r\nLe terme Â« lama Â» est souvent utilisÃ© de maniÃ¨re plus large pour s\'appliquer aux quatre espÃ¨ces animales proches qui constituent la branche sud-amÃ©ricaine des camÃ©lidÃ©s : le lama blanc lui-mÃªme, l\'alpaga, le guanaco et la vigogne (voir le genre lama). Stricto sensu, malgrÃ© quelques croisements, le lama, animal domestique, a pour plus proche cousin le guanaco, animal sauvage, alors que l\'alpaga, animal domestique, a pour plus proche cousin la vigogne, animal sauvage. ');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Animal`
--
ALTER TABLE `Animal`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Animal`
--
ALTER TABLE `Animal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
