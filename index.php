<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Kalam" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
    <title>Faune d'Ariège</title>
</head>

<body>
    <header>
        <h1>Faune d'Ariège</h1>
    </header>

<?php
    if (isset($_REQUEST['choix'])) {
        switch ($_REQUEST['choix']) {

            case "Ajouter":             afficherFormulaire();
                                        break;

            case "Envoyer":             enregistrerFiche();
                                        afficherFiches();
                                        break;

            case "Annuler":             afficherFiches();
                                        break;

            case "Supprimer":           supprimerFiche();
                                        afficherFiches();
                                        break;

            case "Modifier":            afficherFormulaireModif();
                                        break;
            

            case "Update":              modifierFiche();
                                        afficherFiches();
                                        break;


            
        }

    } else {
        afficherFiches();
    }

    /** AFFICHER LES FICHES SUR LA PAGE D'ACCUEIL */
    function afficherFiches(){
        echo ' <form action="index.php" method ="POST" >
         <button type="submit" name="choix" value="Ajouter">Ajouter une fiche</button>
         </form>';

        require "conn_bdd.php";
        $req = 'SELECT * FROM Animal';
        $stmt = $bdd->prepare($req);
        $stmt->execute();
        $fiche = $stmt->fetchAll();

        foreach ($fiche as $info){
            echo '<div class="fiches">';
            echo '<h2>Fiche de l\'animal</h2>';
            echo '<div class="infos">';
            echo  '<p>' . '<strong>Famille :</strong>  ' . $info['famille'] . '</p>';
            echo  '<p>' . '<strong>Espèce :</strong> ' . $info['espece'] . '</p>';
            echo  '<p>' . '<strong>Nom usuel :</strong> ' . $info['nom_usuel'] . '</p>';
            echo  '<p>' . '<strong>Statut :</strong> ' . $info['statut'] . '</p>';
            echo  '<p>' . '<strong>Nombre :</strong> ' . $info['nombre'] . '</p>';
            echo  '<p>' . '<strong>Description :</strong> ' . $info['texte'] . '</p>';
            echo '</div>';
            echo ' <button> <a href="index.php?choix=Supprimer&id=' . $info['id'] . '">Supprimer</button>
            <button> <a href="index.php?choix=Modifier&id=' . $info['id'] . '">Modifier</a> </button>';
            echo '</div>';
        }
    }

    /** AFFICHER LE FORMULAIRE */
    function afficherFormulaire(){

        echo '<section class="section-top">
        <h2>Remplissez ce formulaire pour enregistrer un nouvel animal en Base de Données.</h2>
        
        
        <form action="index.php" method ="POST" >
            <div class="form-top">
                <label>Famille</label>
                <input type="text" name="famille" id="famille" placeholder="Ex : Canidé" required>
                
                <label>Espèce</label>
                <input type="text" name="espece" id="espece" placeholder="Ex : Chien" required>

                <label>Nom usuel</label>
                <input type="text" name="nom" id="nom" placeholder="Ex : Patou" required>

                <label>Statut</label>
                <select name="statut" size="1">
                    <option value="Domestique">Domestique</option>
                    <option value="Semi-domestique">Semi-domestique</option>
                    <option value="Sauvage">Sauvage</option>
                </select>
        
                <label>Nombre estimé en Ariège</label>
                <input type="number" name="nombre" id="nombre" min="0" required>
        
                <label>Description</label>
                <textarea name="texte" id="description" type="text"></textarea> 

                <input type="submit" name="choix" value="Envoyer">

             </form>';

             echo ' <form action="index.php" method ="POST" >
             <button type="submit" name="choix" value="Annuler">Annuler</button>
             </form>
            </div>
            </section>';
    }

    /** ENREGISTRER EN CLIQUANT SUR LE BOUTON  */
    function enregistrerFiche()
    {
        if (isset($_POST['nom'])){
            require "conn_bdd.php";

            $req = 'INSERT INTO `Animal`(`famille`, `espece`, `nom_usuel`, `statut`, `nombre`, `texte`) VALUES (:famille, :espece , :nom, :statut , :nombre, :texte)';
            $stmt = $bdd->prepare($req);
            $stmt->bindValue(":famille", $_POST['famille']);
            $stmt->bindValue(":espece", $_POST['espece']);
            $stmt->bindValue(":nom", $_POST['nom']);
            $stmt->bindValue(":statut", $_POST['statut']);
            $stmt->bindValue(":nombre", $_POST['nombre']);
            $stmt->bindValue(":texte", $_POST['texte']);

            $stmt->execute();
        }
    }
    /** SUPPRIMER LA FICHE SELECTIONNÉE EN CLIQUANT SUR LE BOUTON */
    function supprimerFiche(){
        require "conn_bdd.php";
        $req = 'DELETE  FROM Animal WHERE id=:id';
        $stmt = $bdd->prepare($req);
        $stmt->bindValue(":id", $_REQUEST['id']);
        $stmt->execute();
    }

    /** AFFICHER LE FORMULAIRE PRÉREMPLI POUR LE MODIFIER */
    function afficherFormulaireModif(){
        require "conn_bdd.php";
        $req = 'SELECT * FROM Animal WHERE id=:id';
        $stmt = $bdd->prepare($req);
        $stmt->bindValue(":id", $_REQUEST['id']);
        $stmt->execute();
        $modif = $stmt->fetchAll();
           
            foreach ($modif as $value_modif) {
                echo'<section class="section-top">
                <h2>Remplissez ce formulaire pour enregistrer un nouvel animal en Base de Données.</h2>
                
                
                <form action="index.php" method ="POST" >
                    <div class="form-top">
                    <input type="text" name="id" id="id" value="'.$value_modif[0].'" >
                        <label>Famille</label>
                        <input type="text" name="famille" id="famille" placeholder="Ex : Canidé" value="'.$value_modif[1].'" required>
                        
                        <label>Espèce</label>
                        <input type="text" name="espece" id="espece" placeholder="Ex : Chien" value="'.$value_modif[2].'" required>
        
                        <label>Nom usuel</label>
                        <input type="text" name="nom" id="nom" placeholder="Ex : Patou" value="'.$value_modif[3].'" required>
        
                        <label>Statut</label>
                        <select name="statut" size="1" value="'.$value_modif[4].'">
                            <option value="Domestique">Domestique</option>
                            <option value="Semi-domestique">Semi-domestique</option>
                            <option value="Sauvage">Sauvage</option>
                        </select>
                
                        <label>Nombre estimé en Ariège</label>
                        <input type="number" name="nombre" id="nombre" min="0" value="'.$value_modif[5].'" required>
                
                        <label>texte</label>
                        <textarea name="texte" id="texte" type="text" >'.$value_modif[6].'</textarea> 
                
                        <input type="submit" name="choix" value="Update">
        
                     </form>';
        
                     echo ' <form action="index.php" method ="POST" >
                     <button type="submit" name="choix" value="Annuler">Annuler</button>
                     </form>
                    </div>
                    </section>';
        }
    }

    /** ENREGISTRER LES MODIFICATIONS EN CLIQUANT SUR LE BOUTON */
    function modifierFiche(){
        require "conn_bdd.php";
        $req = "UPDATE Animal SET famille=:famille, espece=:espece, nom_usuel=:nom , statut=:statut , nombre=:nombre, `texte`=:texte WHERE id=:id";
        $stmt = $bdd->prepare($req);
        $stmt->bindValue(":id", $_REQUEST['id']);
        $stmt->bindValue(":famille", $_POST['famille']);
        $stmt->bindValue(":espece", $_POST['espece']);
        $stmt->bindValue(":nom", $_REQUEST['nom']);
        $stmt->bindValue(":statut", $_POST['statut']);
        $stmt->bindValue(":nombre", $_POST['nombre']);
        $stmt->bindValue(":texte", $_POST['texte']);
        $stmt->execute();
    }
?>

</body>
</html>